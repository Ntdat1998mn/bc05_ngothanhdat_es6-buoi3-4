var listNewTask = [];
var listCompletedTask = [];
let id = 0;
class MyNewTask {
  constructor(content, id) {
    (this.content = content), (this.id = id); //, (this.isComplete = false);
  }
}
/* class MyCompletedTask extends MyNewTask {
  constructor(content, id) {
    super(content, id);
   // this.isComplete = true;
  }
} */
const indexOfNewTask = document.querySelector("#todo.todo");
const indexOfCompletedTask = document.querySelector("#completed.todo");
function layThongTinTuForm() {
  var myContent = document.getElementById("newTask").value;
  id++;
  var task = new MyNewTask(myContent, id);
  return task;
}

// Render danh sach
function renderListNewTask(list) {
  indexOfNewTask.innerHTML = list
    .map((item) => {
      return `
            <li id="${item.id}">
                ${item.content}<span></span>
                <div class="buttons">
                  <button class="remove" onclick="remove(${item.id})">
                    <i class="fa fa-trash-alt"></i>
                  </button>
                  <button class="complete" onclick="check(${item.id})">
                    <i class="fa fa-check-circle"></i>
                  </button>
                </div>
            </li>
        `;
    })
    .join("");
}
function renderListCompletedTask(list) {
  indexOfCompletedTask.innerHTML = list
    .map((item) => {
      return `
            <li id="${item.id}">
                <span>${item.content}</span>
                <div class="buttons">
                  <button class="remove" onclick="remove(${item.id})">
                    <i class="fa fa-trash-alt"></i>
                  </button>
                  <button class="complete" onclick="uncheck(${item.id})">
                    <span><i class="fa fa-check-circle"></i></span>
                  </button>
                </div>
            </li>
        `;
    })
    .join("");
}
// Them task
var addTask = (document.getElementById("addItem").onclick = function () {
  var task = layThongTinTuForm();
  listNewTask.push(task);
  renderListNewTask(listNewTask);
});

// Danh dau da hoan thanh task
function check(idOfList) {
  listNewTask = listNewTask.filter((item) => {
    if (item.id == idOfList) {
      listCompletedTask.push(item);
    }
    return item.id !== idOfList;
  });
  renderListNewTask(listNewTask);
  renderListCompletedTask(listCompletedTask);
}
// Danh dau lai task chua hoan thanh
function uncheck(idOfList) {
  listCompletedTask = listCompletedTask.filter((item) => {
    if (item.id == idOfList) {
      listNewTask.push(item);
    }
    return item.id !== idOfList;
  });
  renderListCompletedTask(listCompletedTask);
  renderListNewTask(listNewTask);
}

// Xoá task
function remove(idOfList) {
  document.getElementById(`${idOfList}`).remove();
  listNewTask = listNewTask.filter((item) => {
    return item.id !== idOfList;
  });

  listCompletedTask = listCompletedTask.filter((item) => {
    return item.id !== idOfList;
  });
}

// Xắp xếp

document.getElementById("two").onclick = () => {
  listNewTask.sort((a, b) => {
    return a.content.localeCompare(b.content);
  });
  listCompletedTask.sort((a, b) => {
    return a.content.localeCompare(b.content);
  });
  renderListNewTask(listNewTask);
  renderListCompletedTask(listCompletedTask);
};
document.getElementById("three").onclick = () => {
  listNewTask.sort((b, a) => {
    return a.content.localeCompare(b.content);
  });
  listCompletedTask.sort((b, a) => {
    return a.content.localeCompare(b.content);
  });
  renderListNewTask(listNewTask);
  renderListCompletedTask(listCompletedTask);
};
